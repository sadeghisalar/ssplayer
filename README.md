## A Concept Player Made With Vuejs
### Inspired By @mayakoeva (dribbble)
#### Demo : <https://sadeghisalar.gitlab.io/ssplayer>
---

![](/screenshots/1.png)

## Clone Repository
```
git clone git@gitlab.com:sadeghisalar/ssplayer.git
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
